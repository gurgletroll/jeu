import './style.css'

let perso = document.querySelector<HTMLElement>('#perso');
let camion = document.querySelector<HTMLElement>('#camion');
let os = document.querySelector<HTMLElement>('#os');
let helico = document.querySelector<HTMLElement>('#helico');
const canvas = document.querySelector<HTMLCanvasElement>('#canvas1');
const ctx = canvas.getContext('2d');
const CANVAS_WIDTH = canvas.width = 600;
const CANVAS_HEIGHT = canvas.height = 600;
const playerImage = new Image();
playerImage.src = 'src/shadow_dog.png'
const spriteWidth = 575;
const spriteHeight = 523;

let gameFrame = 0;
let frameX = 0;
let frameY = 3;
let score = 0;




setInterval(resetObstacle, 1500);
animate();
boucleJeu();



function animate() {

  ctx.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);


  ctx.drawImage(playerImage, frameX * spriteWidth, frameY * spriteHeight, spriteWidth, spriteHeight, 0, 0, spriteWidth, spriteHeight);

  if (gameFrame % 3 == 0) {


    if (frameX < 8) {
      frameX++;
    } else {
      frameX = 0;
    }
  }

  gameFrame++;

  requestAnimationFrame(animate);


}




function checkCollision(element1: Element, element2: Element) {
  const rect1 = element1.getBoundingClientRect();
  const rect2 = element2.getBoundingClientRect();

  return !(
    rect1.right < rect2.left ||
    rect1.left > rect2.right ||
    rect1.bottom < rect2.top ||
    rect1.top > rect2.bottom
  );
}




function resetObstacle() {
  let bol = Math.random() < 0.5;
  camion.style.display = 'none';
  helico.style.display = 'none';
  os.style.display = 'none';




  if (bol) {

    os.style.display = 'block';

    os.style.left = '800px';
  } else {


    camion.style.display = 'block';
    helico.style.display = 'block';
    camion.style.left = '800px';
    helico.style.left = '800px';

  }

  boucleJeu();
  console.log(bol);


}



function boucleJeu() {

  if (checkCollision(perso, camion)) {
    console.log('paf le chien ');
    resetObstacle();
  }
  else if (checkCollision(perso, os)) {
    os.style.display = 'none';
    console.log('bel os ');
    setTimeout(resetObstacle, 1500);

  }
  
  

}


let touchePressee = 0;

document.body.addEventListener("keydown", (event) => {

  if (event.key === 'ArrowUp' && touchePressee === 0) {
    touchePressee = Date.now();
  }
});

document.body.addEventListener("keyup", (event) => {
  if (event.key === 'ArrowUp' && touchePressee !== 0) {
    const dureePression = Date.now() - touchePressee;
    touchePressee = 0;


    if (dureePression > 300) {
      perso.classList.add('saut');
    } else {
      perso.classList.add('saut2');
    }


    setTimeout(function () {
      perso.classList.remove('saut');
      perso.classList.remove('saut2');
    }, 500);
    console.log(dureePression);
  }
});





























